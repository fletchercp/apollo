package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"apollo/environment"
	"apollo/search"

	"github.com/julienschmidt/httprouter"
)

// Health returns the Health of the cluster
func Health(env *environment.Env, w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.WriteHeader(200)
}

// CreateIndex creates a new Index
func CreateIndex(env *environment.Env, w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

	w.WriteHeader(http.StatusCreated)
}

// DeleteIndex creates a new Index
func DeleteIndex(env *environment.Env, w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.WriteHeader(http.StatusNotImplemented)
}

// CreateDocument creates a new Document in an Index
func CreateDocument(env *environment.Env, w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	//documentID := ps.ByName("document_id")
	idxName := ps.ByName("name")
	idx := env.IM.Index(idxName)
	if idx == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	var data interface{}
	content, err := ioutil.ReadAll(r.Body)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = json.Unmarshal(content, &data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}

	newDocument := search.NewDocument(0, "", data.(map[string]interface{}))
	idx.AddDocument(newDocument)
	w.WriteHeader(http.StatusCreated)
	return
}

// DeleteDocument creates a new Document
func DeleteDocument(env *environment.Env, w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.WriteHeader(http.StatusNotImplemented)
}

// SearchIndex searches an index for a term
func SearchIndex(env *environment.Env, w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	idxName := ps.ByName("name")
	queryValues := r.URL.Query()
	q := queryValues.Get("q")
	idx := env.IM.Index(idxName)
	if idx == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	sr := idx.Search(q)
	resp, err := json.Marshal(sr)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(resp)
}

// CreatePrimaryShard creates a primary shard for an index on this node
func CreatePrimaryShard(env *environment.Env, w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

	w.WriteHeader(http.StatusCreated)
}

// RegisterShardRequest is a struct that holds a request to register a shard
type RegisterShardRequest struct {
	UUID      string
	Owner     string
	ShardType int
	Index     string
}

// Cat lists out various resources`
func Cat(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.WriteHeader(http.StatusNotImplemented)
}
