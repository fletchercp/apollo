package api

type CarMake struct {
	MakeID       string `json:"make_id"`
	MakeDisplay  string `json:"make_display"`
	MakeIsCommon string `json:"make_is_common"`
	MakeCountry  string `json:"make_country"`
}

/*

func TestHealth(t *testing.T) {
	router := httprouter.New()
	router.GET("/health", Health)
	req, _ := http.NewRequest("GET", "/health", nil)
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Health check failed")
	}
}

func TestCreateIndexAPI(t *testing.T) {
	im = NewIndexManager()
	router := httprouter.New()
	router.POST("/index/:name", CreateIndex)
	req, _ := http.NewRequest("POST", "/index/apitest", nil)
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("Create Index API check failed")
	}
}

func TestCreateDocumentAPI(t *testing.T) {
	im = NewIndexManager()
	router := httprouter.New()
	router.POST("/index/:name", CreateIndex)
	router.POST("/index/:name/document/:doc_name", CreateDocument)
	req, _ := http.NewRequest("POST", "/index/apitest", nil)
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("Create Index API check failed")
	}
	// Now test document creation
	data, _ := json.Marshal(testDocData)
	req, _ = http.NewRequest("POST", "/index/apitest/document/testdoc", bytes.NewBuffer(data))
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("Create Document API check failed")
	}
}

func TestSearchAPI(t *testing.T) {
	im = NewIndexManager()
	router := httprouter.New()
	router.POST("/index/:name", CreateIndex)
	router.POST("/index/:name/document/:doc_name", CreateDocument)
	router.GET("/index/:name", SearchIndex)
	req, _ := http.NewRequest("POST", "/index/apitest", nil)
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("Create Index API check failed")
	}
	// Now test document creation
	data, _ := json.Marshal(testDocData)
	req, _ = http.NewRequest("POST", "/index/apitest/document/testdoc", bytes.NewBuffer(data))
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("Create Document API check failed")
	}

	// Now test searching
	req, _ = http.NewRequest("GET", "/index/apitest?q=test", nil)
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Search API check failed")
	}
}

func TestDeleteDocumentAPI(t *testing.T) {
	im = NewIndexManager()
	router := httprouter.New()
	router.POST("/index/:name", CreateIndex)
	router.POST("/index/:name/document/:doc_name", CreateDocument)
	router.DELETE("/index/:name/:doc_name", DeleteDocument)
	req, _ := http.NewRequest("POST", "/index/apitest", nil)
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("Create Index API check failed")
	}

	// Now test document creation
	data, _ := json.Marshal(testDocData)
	req, _ = http.NewRequest("POST", "/index/apitest/document/testdoc", bytes.NewBuffer(data))
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("Create Document API check failed")
	}

	// Now test searching
	req, _ = http.NewRequest("DELETE", "/index/apitest/testdoc", nil)
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusNotImplemented {
		t.Errorf("Delete API check failed. Response code was: %v", rr.Code)
	}
}

func TestDeleteIndexAPI(t *testing.T) {
	im = NewIndexManager()
	router := httprouter.New()
	router.POST("/index/:name", CreateIndex)
	router.POST("/index/:name/document/:doc_name", CreateDocument)
	router.DELETE("/index/:name", DeleteIndex)
	req, _ := http.NewRequest("POST", "/index/apitest", nil)
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("Create Index API check failed")
	}

	req, _ = http.NewRequest("DELETE", "/index/apitest", nil)
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusNotImplemented {
		t.Errorf("Delete Index API check failed. Response code was: %v", rr.Code)
	}
}

func TestIntegrationCarMakes(t *testing.T) {
	im = NewIndexManager()
	router := httprouter.New()
	router.GET("/health", Health)
	router.POST("/index/:name", CreateIndex)
	router.DELETE("/index/:name", DeleteIndex)
	router.POST("/index/:name/document", CreateDocument)
	router.DELETE("/index/:name/:document_id", DeleteDocument)
	router.GET("/index/:name", SearchIndex)

	raw, err := ioutil.ReadFile("test_data/car_makes.json")
	if err != nil {
		t.Fatalf("Error loading car makes data file: %v", err)
	}
	var m []CarMake
	json.Unmarshal(raw, &m)

	req, _ := http.NewRequest("POST", "/index/carmakes", nil)
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)

	for _, make := range m {
		postData, _ := json.Marshal(make)
		req, _ = http.NewRequest("POST", "/index/carmakes/document", bytes.NewBuffer(postData))
		rr = httptest.NewRecorder()
		router.ServeHTTP(rr, req)
	}

	req, _ = http.NewRequest("GET", "/index/carmakes?q=alpina", nil)
	rr = httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	log.Printf("Search test result: %v", rr.Body)

}
*/
