package api

import (
	"apollo/environment"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

// JoinClusterRequest is a request from a client to join a cluster
type JoinClusterRequest struct {
	ID string `json:"id"`
	IP string `json:"ip"`
}

// JoinClusterHandler wraps a handler that processes a Join Cluster request
type JoinClusterHandler struct {
	Env *environment.Env
}

func (jch *JoinClusterHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		log.Printf("Processing join request")
		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		jcr := &JoinClusterRequest{}
		err = json.Unmarshal(data, jcr)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		NT := jch.Env.NT
		NT.AddNode(jcr.ID, jcr.IP)
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
}

// HeartbeatPayload contains the ID of the node checking in
type HeartbeatPayload struct {
	ID string `json:"id"`
}

// HeartbeatHandler wraps a handler that handles a heartbeat from a Node
type HeartbeatHandler struct {
	Env *environment.Env
}

func (hbh *HeartbeatHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		log.Printf("Handling heartbeat")
		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Printf("Error reading request body: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		hbp := &HeartbeatPayload{}
		err = json.Unmarshal(data, hbp)
		if err != nil {
			log.Printf("Error unmarshaling heartbeat payload: %v", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		NT := hbh.Env.NT
		node := NT.Node(hbp.ID)
		if node == nil {
			log.Printf("Unknown node attempted heartbeat: %v", hbp.ID)
			w.WriteHeader(http.StatusNotFound)
			return
		}
		node.LastHeard = time.Now()
		log.Printf("Node lastHeard updated")
		w.WriteHeader(http.StatusOK)
		return
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
}
