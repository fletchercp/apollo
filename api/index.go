package api

import (
	"apollo/environment"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

// IndexHandler handles an Index request
type IndexHandler struct {
	Env *environment.Env
}

type indexPayload struct {
	Name string `json:"name"`
}

func (ih *IndexHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		ih.POST(w, r)
		return
	}
	w.WriteHeader(http.StatusMethodNotAllowed)
}

// POST handles a Post request
func (ih *IndexHandler) POST(w http.ResponseWriter, r *http.Request) {
	log.Printf("Processing join request")
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	payload := &indexPayload{}
	err = json.Unmarshal(data, payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	idx := ih.Env.IM.Index(payload.Name)
	if idx != nil {
		w.WriteHeader(http.StatusConflict)
		return
	}

	idx, err = ih.Env.IM.CreateIndex(payload.Name)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	for _, shard := range idx.Shards {
		if shard.Node == "Unassigned" {
			node := ih.Env.NT.RandomNode()
			err = node.AssignShard(shard)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
		}
	}
	w.WriteHeader(http.StatusCreated)
}
