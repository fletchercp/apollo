package api

import (
	"apollo/environment"
	"apollo/search"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

// ShardHandler handles an Index request
type ShardHandler struct {
	Env *environment.NodeEnvironment
}

type shardPayload struct {
	ShardID   string `json:"shard_id"`
	Index     string `json:"index"`
	ShardType int    `json:"shard_type"`
}

func (sh *ShardHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		sh.POST(w, r)
		return
	}
	w.WriteHeader(http.StatusMethodNotAllowed)
	return
}

// POST handles a POST request
func (sh *ShardHandler) POST(w http.ResponseWriter, r *http.Request) {
	log.Printf("Handling Shard POST")
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var payload shardPayload
	err = json.Unmarshal(data, &payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var nst search.ShardType
	log.Printf("Checking shard type: %v", payload)
	if payload.ShardType == search.Primary {
		nst = search.Primary
	} else if payload.ShardType == search.Replica {
		nst = search.Replica
	} else {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	shard := search.NewShard(nst, nil, sh.Env.ID)
	if shard == nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	sh.Env.ShardTracker.AddShard(payload.Index, shard)
	log.Printf("Shard added")
	w.WriteHeader(http.StatusCreated)
	return
}
