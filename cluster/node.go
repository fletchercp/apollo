package cluster

import (
	"apollo/search"
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
)

// NodeStatus tracks the status of the Node
type NodeStatus int

const (
	// OK means the node is checking in
	OK = iota
	// Dead means the Node has not checked in for at least 10 seconds
	Dead
)

// Node represents a physical server that can run many virtual nodes
type Node struct {
	ID        string
	IP        string
	LastHeard time.Time
	Shards    []*search.Shard
}

// NewNode creates and returns a new Node
func NewNode(id, ip string) *Node {
	return &Node{
		ID:        id,
		IP:        ip,
		LastHeard: time.Now(),
	}
}

type assignShardRequest struct {
	ShardID   string `json:"shard_id"`
	Index     string `json:"index"`
	ShardType int    `json:"shard_type"`
}

// AssignShard assigns a shard to this Node
func (n *Node) AssignShard(s *search.Shard) error {
	log.Printf("Assigning shard %v to node %v", s.ShardID, n.ID)
	nodeAddr := "http://" + n.IP + ":8999/shard"
	sr := &assignShardRequest{
		ShardID:   s.ShardID.String(),
		Index:     s.Index.Name,
		ShardType: int(s.ShardType),
	}
	data, err := json.Marshal(sr)
	if err != nil {
		return err
	}
	log.Printf("Making request to node: %v", nodeAddr)
	resp, err := http.Post(nodeAddr, "application/json", bytes.NewBuffer(data))
	if err != nil {
		log.Printf("Error from node: %v", err)
		return err
	}
	log.Printf("Request done: %v", resp.StatusCode)
	if resp.StatusCode == http.StatusCreated {
		log.Printf("Shard created")
		return nil
	}
	return fmt.Errorf("Invalid response from node: %d", resp.StatusCode)
}
