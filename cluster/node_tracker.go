package cluster

import (
	"log"
	"math/rand"
	"sync"
	"time"
)

// NodeTracker is a data structure that tracks Nodes that have registered to the
// cluster
type NodeTracker struct {
	Nodes     map[string]*Node
	NodeNames []string
	nodeMutex *sync.Mutex
}

// NewNodeTracker creates and returns a new Node tracker
func NewNodeTracker() *NodeTracker {
	return &NodeTracker{
		Nodes:     make(map[string]*Node, 0),
		NodeNames: make([]string, 0),
		nodeMutex: &sync.Mutex{},
	}
}

// AddNode adds a node to the NodeTracker
func (nt *NodeTracker) AddNode(id, ip string) {
	nn := NewNode(id, ip)
	nt.nodeMutex.Lock()
	nt.Nodes[id] = nn
	nt.NodeNames = append(nt.NodeNames, id)
	nt.nodeMutex.Unlock()
}

// Node returns a Node in the cluster
func (nt *NodeTracker) Node(id string) *Node {
	nt.nodeMutex.Lock()
	defer nt.nodeMutex.Unlock()
	if val, ok := nt.Nodes[id]; ok {
		return val
	}
	return nil
}

// RandomNode returns a Random Node in the cluster
func (nt *NodeTracker) RandomNode() *Node {
	return nt.Nodes[nt.NodeNames[rand.Intn(len(nt.NodeNames))]]
}

// CheckLastHeard watches for a node to stop checking in and marks it as down
func (nt *NodeTracker) CheckLastHeard() {
	log.Println("Starting CheckLastHeard in NodeTracker...")
	ticker := time.NewTicker(time.Second * 10)
	for _ = range ticker.C {
		nt.nodeMutex.Lock()
		for _, v := range nt.Nodes {
			diff := time.Now().Sub(v.LastHeard)
			if diff >= time.Second*10 {
				log.Printf("Have not heard from node: %v", v.ID)
			}
		}
		nt.nodeMutex.Unlock()
	}
}
