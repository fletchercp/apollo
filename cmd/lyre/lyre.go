package main

import (
	"apollo/api"
	"apollo/config"
	"apollo/environment"
	"apollo/search"
	"flag"
	"fmt"
	"net/http"
	"os"
	"strconv"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
)

var portNum *string

func main() {

	portNum := flag.Int("port", 9000, "Port number Lyre listens on")

	log.SetFormatter(&log.JSONFormatter{})
	err := envconfig.Process("hermes", &config.S)
	if err != nil {
		log.Fatalf("Error processing config: %s", err)
	}

	// Load the IndexManager
	log.Printf("Loading IndexManager...")
	im, err := search.LoadIndexManager()
	if err != nil {
		log.Fatalf("Error loading existing IndexManager: %v", err)
	}

	if os.IsNotExist(err) {
		log.Printf("IndexManager does not exist, creating...")
		im = search.NewIndexManager()
		search.SaveIndexManager(*im)
	}

	Env := environment.New(im)
	http.Handle("/join", &api.JoinClusterHandler{Env: Env})
	http.Handle("/heartbeat", &api.HeartbeatHandler{Env: Env})
	http.Handle("/index", &api.IndexHandler{Env: Env})

	fmt.Printf("Starting server. Listening on port %d\n", *portNum)
	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(*portNum), nil))
}
