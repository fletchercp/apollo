package config

// S is our config spec
var S Specification

// Specification holds various config values
type Specification struct {
	Debug             bool
	DataDir           string `default:"/var/lib/hermes/"`
	Scheme            string `default:"http"`
	DocumentsPerShard int    `default:"100"`
	Primary           int    `default:"5"`
	Replica           int    `default:"2"`
	LyreServer        string `default:"localhost"`
	LyrePort          int    `default:"9000"`
	NodeID            string `default:""`
}
