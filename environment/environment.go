package environment

import (
	"apollo/cluster"
	"apollo/search"
)

// Env wraps all the structures that we'll need to pass around to various
// packages
type Env struct {
	IM *search.IndexManager
	NT *cluster.NodeTracker
}

// New creates and returns a new Env
func New(im *search.IndexManager) *Env {
	newEnv := &Env{
		IM: im,
		NT: cluster.NewNodeTracker(),
	}
	go newEnv.NT.CheckLastHeard()
	return newEnv
}

// Null returns an Env with nil values to everything
func Null() *Env {
	return &Env{}
}
