package environment

import (
	"apollo/search"
	"bytes"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"
	"time"
)

// NodeEnvironment maintains the environment for a specific Node
type NodeEnvironment struct {
	ID           string
	IP           string
	LyreAddress  string
	LyrePort     int
	ShardTracker *search.ShardTracker
}

// NewNodeEnvironment creates a new NodeEnvironment
func NewNodeEnvironment(ID, IP, LyreAddress string, LyrePort int) *NodeEnvironment {
	return &NodeEnvironment{
		ID:           ID,
		IP:           IP,
		LyreAddress:  LyreAddress,
		LyrePort:     LyrePort,
		ShardTracker: search.NewShardTracker(),
	}
}

type heartbeatPayload struct {
	ID string `json:"id"`
}

// CheckIn checks in with the Lyre server
func (ne *NodeEnvironment) CheckIn() error {
	log.Println("Beginning CheckIn...")
	lyreAddr := "http://" + ne.LyreAddress + ":" + strconv.Itoa(ne.LyrePort) + "/heartbeat"
	ticker := time.NewTicker(time.Second * 5)
	for _ = range ticker.C {
		log.Printf("Ticker!")
		payload := &heartbeatPayload{
			ID: ne.ID,
		}
		json, err := json.Marshal(payload)
		if err != nil {
			return err
		}
		resp, err := http.Post(lyreAddr, "application/json", bytes.NewBuffer(json))
		if err != nil {
			log.Printf("Error checking in: %v", err)
		}
		log.Printf("Check response: %v", resp.StatusCode)
		resp.Body.Close()
	}
	log.Printf("Checkin goroutine done")
	return nil
}

// joinClusterRequest is a request from a client to join a cluster
type joinClusterRequest struct {
	ID string `json:"id"`
	IP string `json:"ip"`
}

// Join joins the node to a Lyre server
func (ne *NodeEnvironment) Join() error {
	lyreAddr := "http://" + ne.LyreAddress + ":" + strconv.Itoa(ne.LyrePort) + "/join"
	data := &joinClusterRequest{
		ID: ne.ID,
		IP: ne.IP,
	}
	json, err := json.Marshal(data)
	if err != nil {
		return err
	}
	resp, err := http.Post(lyreAddr, "application/json", bytes.NewBuffer(json))
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode == http.StatusOK {
		go ne.CheckIn()
		return nil
	}
	return errors.New("Incorrect return code")
}
