package search

var stopWords []string

func init() {
	stopWords = []string{"a", "an", "and", "are", "as", "at", "be", "but", "by",
		"for", "if", "in", "into", "is", "it", "no", "not", "of", "on", "or", "such",
		"that", "the", "their", "then", "there", "these", "they", "this", "to", "was",
		"will", "with"}
}

// Analyze analyzes the terms
func Analyze(words *[]string) *[]string {
	var result []string
	for _, word := range *words {
		isStop := false
		for _, sm := range stopWords {
			if sm == word {
				isStop = true
				break
			}
		}
		if isStop == false {
			result = append(result, word)
		}
	}
	return &result
}
