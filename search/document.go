package search

import (
	"regexp"
	"strings"

	uuid "github.com/satori/go.uuid"
)

// Document is a collection of text
type Document struct {
	ID        uuid.UUID
	Name      string
	Content   string
	Index     string
	IndexName string
	Positions map[string][]int
	Fields    map[string][]string
}

// NewDocument creates and returns a new Document
func NewDocument(id int, name string, m map[string]interface{}) *Document {
	newDoc := &Document{
		ID:        uuid.NewV4(),
		Name:      name,
		Positions: make(map[string][]int),
		Fields:    make(map[string][]string),
	}

	for k, v := range m {
		val := v.(string)
		wsStripper, err := regexp.Compile(`[\W_]+`)
		if err != nil {
			return nil
		}
		val = wsStripper.ReplaceAllString(val, " ")
		newDoc.Fields[k] = strings.Split(val, " ")
	}
	return newDoc
}

// AddTerm adds a Term to the document
func (d *Document) AddTerm(f, t string) {
	d.Fields[f] = append(d.Fields[f], t)
}

// HasTerm checks if a Document has a specific Term
func (d *Document) HasTerm(f, t string) bool {
	for _, term := range d.Fields[f] {
		if t == term {
			return true
		}
	}
	return false
}

// LoadDocument loads a document
func LoadDocument(id, indexName string) (*Document, error) {
	/*
		var d Document
		var dirPath string

		file, err := os.Open(dirPath + id + ".gob")
		if err != nil {
			return nil, err
		}

		decoder := gob.NewDecoder(file)
		err = decoder.Decode(&d)
		file.Close()
		return &d, err
	*/
	return nil, nil
}

// SaveDocument saves a Document to disk as a gob
func SaveDocument(d Document, dirPath string) error {
	/*
		d.IndexName = d.Index
		err := os.MkdirAll(dirPath, 0700)
		if err != nil {
			return err
		}
		file, err := os.Create(dirPath + strconv.Itoa(d.ID) + ".gob")
		if err != nil {
			return err
		}

		encoder := gob.NewEncoder(file)
		err = encoder.Encode(d)
		if err != nil {
			fmt.Printf("Error: %v", err)
		}
		file.Close()
		return err
	*/
	return nil
}

// ValidateDocumentName validates that a document name is, well, valid
func ValidateDocumentName(name string) bool {
	// TODO: Implement actual validation
	return true
}
