package search

import "testing"

var testDocData map[string]interface{}

func init() {
	testDocData = make(map[string]interface{})
	testDocData["message"] = "test message"
}

func TestCreateDocument(t *testing.T) {
	tf := NewDocument(0, "test", testDocData)
	if tf == nil {
		t.Fatal("Test document is nil")
	}
}

func TestAddTermToDocument(t *testing.T) {
	tf := NewDocument(0, "test", testDocData)
	tf.AddTerm("message", "testterm")
	result := tf.HasTerm("message", "testterm")
	if result == false {
		t.Fatal("Test term was not found after adding")
	}

	result = tf.HasTerm("message", "unknown")
	if result == true {
		t.Fatal("A term that should not have been found was")
	}
}

/*
func TestExtractTerms(t *testing.T) {

	tf := NewDocument(0, "test")
	tf.extractTerms()
	if len(tf.Terms) != 2 {
		t.Fatal("Term extraction did not have expected number of terms")
	}

}

func TestIndexPositions(t *testing.T) {
	tf := NewDocument(0, "test", "This is a    test")
	tf.extractTerms()
	if len(tf.Positions["test"]) != 1 {
		t.Fatal("A term was not correctly indexed")
	}

	if tf.Positions["This"][0] != 0 {
		t.Fatal("A term was indexed at the incorrect location")
	}
}

func TestTermPositions(t *testing.T) {
	tf := NewDocument(0, "test", "This is a    test")
	tf.extractTerms()
	p := tf.TermPositions("This")
	if len(p) == 0 {
		t.Fatalf("A position for a test term was not found but should have been")
	}
}
*/
