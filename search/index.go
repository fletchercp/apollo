package search

import (
	"apollo/config"

	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
)

// Index is a collection of Documents
type Index struct {
	Name      string
	IndexPath string
	Shards    []*Shard
	Primary   int
	Replicas  int
	ID        uuid.UUID
}

// InvertedIndex stores the term, freq and docs
type InvertedIndex struct {
	// Holds the terms
	Entries map[string][]*InvertedIndexEntry
}

// InvertedIndexEntry is an entry in the Inverted Index
type InvertedIndexEntry struct {
	// Document ID where the word is found
	Document uuid.UUID
	// Location in the field where the word is found
	Location int
}

// SearchResult contains the results of a search query
type SearchResult struct {
	Hits []*ResultHit
}

// ResultHit contains all the info for a single positive result
type ResultHit struct {
	Index  string              `json:"_index"`
	ID     uuid.UUID           `json:"_id"`
	Source map[string][]string `json:"_source"`
}

// NewIndex creates and returns a new Index
func NewIndex(name string, primary, replica int) *Index {
	newIndex := &Index{
		Name:     name,
		Shards:   make([]*Shard, 0),
		ID:       uuid.NewV4(),
		Primary:  primary,
		Replicas: replica,
	}
	newIndex.IndexPath = config.S.DataDir + "indices/" + newIndex.Name
	log.WithFields(log.Fields{
		"action":         "createIndex",
		"primary_shards": primary,
		"replica_shards": replica,
		"index_name":     name,
	}).Infof("Creating new index")

	for i := 0; i < primary; i++ {
		newShard := NewShard(Primary, newIndex, "Unassigned")
		newIndex.AddShard(newShard)
	}
	return newIndex
}

// AddShard adds a Shard
func (idx *Index) AddShard(s *Shard) {
	idx.Shards = append(idx.Shards, s)
}

// Search searches the index for documents that contain the query
func (idx *Index) Search(query string) *SearchResult {
	sr := &SearchResult{
		Hits: make([]*ResultHit, 0),
	}

	for _, shard := range idx.Shards {
		sr.Hits = append(sr.Hits, shard.Search(query).Hits...)
	}
	return sr
}

// LoadIndex loads a saved Index from disk
func LoadIndex(name string) (*Index, error) {
	/*
		idx := NewIndex(name)
		var dirPath string
		if (Specification{}) == s {
			dirPath = "/tmp/indices/" + name + "/"
		} else {
			dirPath = s.DataDir + "/indices/" + name + "/"
		}

		file, err := os.Open(dirPath + name + ".idx")
		if err != nil {
			return nil, err
		}
		decoder := gob.NewDecoder(file)
		err = decoder.Decode(idx)
		file.Close()
		for d := range idx.DocumentList {
			newDoc, docErr := LoadDocument(strconv.Itoa(d), name)
			if docErr != nil {
				log.Printf("There was an error loading document: %v", err)
			}
			idx.Documents = append(idx.Documents, newDoc)
		}
		return idx, err
	*/
	return nil, nil
}

// SaveIndex saves an Index to disk
func SaveIndex(idx Index) error {
	/*
		// First create the sub-directory in the indices folder
		var dirPath string
		if (Specification{}) == s {
			dirPath = "/tmp/" + idx.IndexPath + "/" + idx.Name + "/"
		} else {
			dirPath = s.DataDir + idx.IndexPath + "/" + idx.Name + "/"
		}
		err := os.MkdirAll(dirPath, 0700)
		if err != nil {
			return err
		}

		// Write the index info file that contains the name and other data
		file, err := os.Create(dirPath + idx.Name + ".idx")
		if err != nil {
			return err
		}

		for _, d := range idx.Documents {
			idx.DocumentList = append(idx.DocumentList, strconv.Itoa(d.ID))
		}
		idx.Documents = nil

		encoder := gob.NewEncoder(file)
		err = encoder.Encode(idx)
		if err != nil {
			fmt.Printf("Error: %v", err)
		}
		file.Close()
		return err
	*/
	return nil
}

// AddDocument adds a Document to the Index
func (idx *Index) AddDocument(d *Document) {
	d.Index = idx.Name
	SaveIndex(*idx)
}
