package search

import (
	"apollo/config"
	"fmt"

	log "github.com/sirupsen/logrus"
)

// IndexManager manages the collection of Indices the server is responsible for
type IndexManager struct {
	indices   map[string]*Index
	IndexList []string
}

// NewIndexManager creates and returns a new IndexManager
func NewIndexManager() *IndexManager {
	return &IndexManager{
		indices:   make(map[string]*Index, 0),
		IndexList: make([]string, 0),
	}
}

// Index returns an existing index, if it exists
func (im *IndexManager) Index(name string) *Index {
	v, ok := im.indices[name]
	if ok == true {
		return v
	}
	return nil
}

// SaveIndexManager saves the IndexManager to disk
func SaveIndexManager(im IndexManager) error {
	/*
		var dirPath string
		if (Specification{}) == s {
			dirPath = "/tmp/indices/"
		} else {
			dirPath = s.DataDir + "indices/"
		}
		err := os.MkdirAll(dirPath, 0700)
		if err != nil {
			return err
		}

		// Write the index info file that contains the name and other data
		file, err := os.Create(dirPath + "indices" + ".gob")
		if err != nil {
			return err
		}

		for k := range im.indices {
			im.IndexList = append(im.IndexList, k)
		}
		im.indices = nil

		encoder := gob.NewEncoder(file)
		err = encoder.Encode(im)
		if err != nil {
			fmt.Printf("Error: %v", err)
		}
		file.Close()
		return err
	*/
	return nil
}

// LoadIndexManager loads an IndexManager from disk
func LoadIndexManager() (*IndexManager, error) {
	idxMgr := &IndexManager{
		indices: make(map[string]*Index),
	}
	return idxMgr, nil
	/*
			var dirPath string
			if (Specification{}) == s {
				dirPath = "/tmp/indices/"
			} else {
				dirPath = s.DataDir + "indices/"
			}
			err := os.MkdirAll(dirPath, 0700)
			if err != nil {
				return nil, err
			}

			file, err := os.Open(dirPath + "indices.gob")
			if err != nil {
				return nil, err
			}

			idxManager := &IndexManager{
				indices: make(map[string]*Index),
			}

			decoder := gob.NewDecoder(file)
			err = decoder.Decode(&idxManager)

			if err != nil {
				fmt.Printf("Error: %v", err)
			}
			file.Close()
			for _, idx := range idxManager.IndexList {
				loadedIdx, err := LoadIndex(idx)
				if err != nil {
					log.Printf("Error loading index %v: %v", idx, err)
				} else {
					idxManager.indices[idx] = loadedIdx
				}
			}

			return idxManager, nil

		return nil, nil
	*/
}

// CreateIndex creates a new Index
func (im *IndexManager) CreateIndex(name string) (*Index, error) {
	v, ok := im.indices[name]
	if ok == true {
		log.WithFields(log.Fields{
			"system": "index_manager",
			"action": "CreateIndex",
		}).Debugf("Index already exists")
		return nil, fmt.Errorf("Index %v already exists", v.Name)
	}
	newIndex := NewIndex(name, config.S.Primary, config.S.Replica)
	//SaveIndex(*newIndex)
	im.indices[name] = newIndex
	log.WithFields(log.Fields{
		"action":  "CreateIndexAPI",
		"name":    name,
		"indices": im.indices,
	}).Debugf("Received API request to create index")
	//SaveIndexManager(*im)
	return newIndex, nil
}

// DeleteIndex deletes an Index
func (im *IndexManager) DeleteIndex(name string) (bool, error) {
	_, ok := im.indices[name]
	if ok != true {
		return false, fmt.Errorf("Index %v does not exist", name)
	}

	// TODO: We should delete the data from disk
	im.indices[name] = nil
	return true, nil
}
