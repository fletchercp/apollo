package search

import (
	"testing"
)

func TestCreateIndexManager(t *testing.T) {
	newIdxMgr := NewIndexManager()
	if newIdxMgr == nil {
		t.Fatalf("Error creating new IndexManager")
	}
}

func TestGetIndex(t *testing.T) {
	newIdxMgr := NewIndexManager()
	idx := newIdxMgr.Index("test")
	if idx != nil {
		t.Fatalf("Got an index from IndexManager when there should have been none")
	}
}

func TestCreateIndexInIndexManager(t *testing.T) {
	newIdxMgr := NewIndexManager()
	_, err := newIdxMgr.CreateIndex("test")
	if err != nil {
		t.Fatalf("Error creating new index: %v", err)
	}

	testIdx := newIdxMgr.Index("test")
	if testIdx == nil {
		t.Fatalf("Failed to retrieve existing Index")
	}

	_, err = newIdxMgr.CreateIndex("test")
	if err == nil {
		t.Fatalf("Was able to create a duplicate index")
	}
}

func TestDeleteIndex(t *testing.T) {
	newIdxMgr := NewIndexManager()
	_, err := newIdxMgr.CreateIndex("test")
	if err != nil {
		t.Fatalf("Error creating new index: %v", err)
	}

	result, err := newIdxMgr.DeleteIndex("test")
	if result != true {
		t.Fatalf("Failed to delete test index")
	}

	if err != nil {
		t.Fatalf("Error deleting index: %v", err)
	}

	_, err = newIdxMgr.DeleteIndex("doesnotexist")
	if err == nil {
		t.Fatalf("Error not found when deleting non-existant index")
	}
}

func TestSaveLoadIndexManager(t *testing.T) {
	newIdxMgr := NewIndexManager()
	err := SaveIndexManager(*newIdxMgr)
	if err != nil {
		t.Fatalf("Error saving Index Manager: %v", err)
	}

	_, err = LoadIndexManager()
	if err != nil {
		t.Fatalf("Error loading saved IndexManager: %v", err)
	}

}
