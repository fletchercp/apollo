package search

import (
	"apollo/config"

	uuid "github.com/satori/go.uuid"
)

// Segment is a piece of an index
type Segment struct {
	Documents   []*Document
	Inverted    map[string]*InvertedIndex
	SegmentID   uuid.UUID
	Shard       *Shard
	SegmentPath string
}

// NewSegment creates and returns a new Segment
func NewSegment(parent *Shard) *Segment {
	newSegment := &Segment{
		Documents: make([]*Document, 0),
		Inverted:  make(map[string]*InvertedIndex),
		SegmentID: uuid.NewV4(),
		Shard:     parent,
	}
	newSegment.SegmentPath = parent.ShardPath + "segments/" + newSegment.SegmentID.String() + ".gob"
	return newSegment
}

// GetDocument gets a document by ID,
func (seg *Segment) GetDocument(id uuid.UUID) *Document {
	for _, d := range seg.Documents {

		if d.ID == id {
			return d
		}
	}
	return nil
}

// NumDocs returns the number of Documents in a Segment
func (seg *Segment) NumDocs() int {
	return len(seg.Documents)
}

// Search searches the index for documents that contain the query
func (seg *Segment) Search(term string) *SearchResult {
	sr := &SearchResult{
		Hits: make([]*ResultHit, 0),
	}
	for field := range seg.Inverted {
		v, ok := seg.Inverted[field].Entries[term]
		if ok == true {
			for _, result := range v {
				rh := &ResultHit{}
				rh.ID = result.Document
				document := seg.GetDocument(result.Document)
				rh.Source = document.Fields
				sr.Hits = append(sr.Hits, rh)
			}
		}
	}
	return sr
}

// AddDocument adds a Document to the Index
func (seg *Segment) AddDocument(d *Document) {
	seg.Documents = append(seg.Documents, d)
	seg.IndexDocument(d)
	SaveDocument(*d, config.S.DataDir+seg.SegmentPath)
}

// IndexDocument indexes the document
func (seg *Segment) IndexDocument(d *Document) {
	for k := range d.Fields {
		// Create the inverted index for the field if needed
		_, ok := seg.Inverted[k]
		if ok == false {
			seg.Inverted[k] = &InvertedIndex{
				Entries: make(map[string][]*InvertedIndexEntry),
			}
		}

		for i, term := range d.Fields[k] {
			entry := &InvertedIndexEntry{
				Document: d.ID,
				Location: i,
			}
			seg.Inverted[k].Entries[term] = append(seg.Inverted[k].Entries[term], entry)
		}
	}
}

// Merge merges one Segment with this one, with the Segment passed in being deleted
func (seg *Segment) Merge(s *Segment) {
	// Merging just means combining Documents and Inverted Indices
	seg.Documents = append(seg.Documents, s.Documents...)
	for k := range s.Inverted {
		_, ok := seg.Inverted[k]
		if ok == false {
			seg.Inverted[k] = s.Inverted[k]
			delete(s.Inverted, k)
		} else {
			for i := range s.Inverted[k].Entries {
				_, termOK := seg.Inverted[k].Entries[i]
				if termOK == false {
					seg.Inverted[k].Entries[i] = s.Inverted[k].Entries[i]
				} else {
					seg.Inverted[k].Entries[i] = append(seg.Inverted[k].Entries[i], s.Inverted[k].Entries[i]...)
				}
			}
		}
		delete(s.Inverted, k)
	}
}

// Save saves a Segment to Disk
func (seg *Segment) Save() {

}
