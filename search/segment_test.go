package search

import "testing"

func TestSegmentMerge(t *testing.T) {
	parentShard := &Shard{}
	seg1 := NewSegment(parentShard)
	seg2 := NewSegment(parentShard)
	newDocument := NewDocument(0, "Test", testDocData)
	newDocumentTwo := NewDocument(1, "Another", testDocData)
	seg1.AddDocument(newDocument)
	seg2.AddDocument(newDocumentTwo)
	seg1.Merge(seg2)
	// TODO: Add a check here
}
