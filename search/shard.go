package search

import (
	"apollo/config"

	"github.com/satori/go.uuid"
)

// ShardType is what type of shard the Shard is
type ShardType int

const (
	// Primary is a primary shard
	Primary = iota
	// Replica is a replica shard
	Replica
)

// Shard is a unit of searchability and horizontal scaling
type Shard struct {
	ShardType         ShardType
	Segments          []*Segment
	Index             *Index
	nextSegmentNumber int
	currentSegment    *Segment
	ShardPath         string
	ShardID           uuid.UUID
	Node              string
}

// NewShard creates and returns a new Shard
func NewShard(shardType ShardType, idx *Index, owner string) *Shard {
	newShard := &Shard{
		ShardType: shardType,
		Segments:  make([]*Segment, 0),
		Index:     idx,
		ShardID:   uuid.NewV4(),
		Node:      owner,
	}
	newShard.ShardPath = idx.IndexPath + idx.Name + "/shards/" + newShard.ShardID.String() + "/"
	newSegment := newShard.CreateSegment()
	newShard.currentSegment = newSegment
	return newShard
}

// CreateSegment and adds a new Segment to this Shard and returns a pointer to the new
// segment
func (shd *Shard) CreateSegment() *Segment {
	shd.nextSegmentNumber = shd.nextSegmentNumber + 1
	newSegment := NewSegment(shd)
	shd.Segments = append(shd.Segments, newSegment)
	return newSegment
}

// AddDocument adds a document to the current Segment
func (shd *Shard) AddDocument(d *Document) {
	shd.currentSegment.AddDocument(d)
	if shd.currentSegment.NumDocs() >= config.S.DocumentsPerShard {
		shd.currentSegment.Save()
		newSegment := shd.CreateSegment()
		shd.currentSegment = newSegment
	}
}

// Search searches all Segments in this Shard
func (shd *Shard) Search(query string) *SearchResult {
	sr := &SearchResult{
		Hits: make([]*ResultHit, 0),
	}

	for _, seg := range shd.Segments {
		sr.Hits = append(sr.Hits, seg.Search(query).Hits...)
	}
	return sr
}
