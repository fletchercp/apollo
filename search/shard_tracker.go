package search

// ShardTracker tracks all the Shards assigned to a Node
type ShardTracker struct {
	Shards map[string][]*Shard
}

// NewShardTracker creates and returns a new Shard Tracker
func NewShardTracker() *ShardTracker {
	return &ShardTracker{
		Shards: make(map[string][]*Shard, 0),
	}
}

// AddShard adds a shard to the ShardTracker
func (st *ShardTracker) AddShard(idx string, s *Shard) error {
	st.Shards[idx] = append(st.Shards[idx], s)
	return nil
}
